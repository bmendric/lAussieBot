const constants = require.main.require("./config/constants.js");
const rp = require("request-promise");

/**
 * Wrapper class for generating /r/gonewild comment/compliments
 * @class
 */
class Compliments {
    /** @constructs */
    constructor() {
        this.comments = [];
        this.lastRefresh = 0;
        this.refresh();
    }

    /**
     * Updates the internal list of compliments to choose from
     * @private
     * @returns {Promise<void>}
     */
    refresh() {
        // Checking if a refresh is needed
        if (this.comments.length && Date.now() - this.lastRefresh <= constants.epoch.hour) {
            return Promise.resolve();
        }

        // Need refresh
        var this_ = this;
        this.comments = [];

        return rp({uri: "https://www.reddit.com/r/gonewild/comments.json?limit=20", json: true})
        .then(result => {
            // Parsing returned data
            for (let child of result.data.children) {
                let data = child.data.body;

                if (!(data.match(/\/?r\/(?:gonewild)?/i) || data.match(/beep boop/i) || data.match(/\/?u\//i))) {
                    this_.comments.push(data);
                }
            }

            // Documenting the refresh
            this_.lastRefresh = new Date();

            return Promise.resolve();
        });
    }

    /**
     * Returns a random compliment from the internal cache.
     * Updates the cache if needed, beforehand.
     * @returns {Promise<String>} A Promise for the random compliment
     */
    getCompliment() {
        var this_ = this;

        return this.refresh()
        .then(() => {
            // Picking a random number
            var rand = Math.floor(Math.random() * this_.comments.length);

            // Grabbing the compliment
            var output = this_.comments[rand];

            // Removing the comment
            this_.comments.splice(rand, 1);

            // Updating the freshness value
            this_.fresh -= 1;
            
            return Promise.resolve(output);
        });
    }
};

module.exports = Compliments;