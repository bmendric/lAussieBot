const config = require(`./config/config.json`);
const commandList = require("./commands/commandList.js");
const Discord = require("discord.js");
const Manager = require("./Manager.js");

const client = new Discord.Client();
const manager = new Manager(client, config, commandList);
