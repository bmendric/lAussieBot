const constants = require("./config/constants.js");

/**
 * Options for configuring commands
 * @typedef {Object} CommandOptions
 * @property {Integer} cid Globally unique command identifier; subcommand inherit the cid of the parent
 * @property {String} [parent] The name of the parent command (only if a subcommand)
 * @property {String} name The name of the command
 * @property {Array<String>} [aliases] Any aliases the command may have
 * @property {String} usage How the command should be used
 * @property {String} description A short description of what the command does
 * @property {String} [fullDescription=description] A full-length description of what the command does and how to use it
 * @property {Integer} [category=permissions.public] Permissions category the commands belongs to; defined in ./config/constants.js
 * @property {Boolean} [deleteCommand=false] Delete the original command message
 * @property {Boolean} [noArgs=false] If arguments are sent into the command function
 * @property {Boolean} [noClean=false] Cleaning refers to the removal of empty args
 * @property {Object} [arguments] Configuration options for auto-checking arguments
 * @property {Integer} [arguments.min] Minimum number of arguments; must be > 0
 * @property {Integer} [arguments.max] Maximum number of arguments; must be > 0
 * @property {Integer} [arguments.exact] Check for an exact number of arguments; must be > 0
 * @property {Boolean} [arguments.none] Ensures no arguments are provided to the command
 * @property {Array<Integer>} [help] List of all permission categories the command should not appear in the help text of; if an empty array is given, the command is not shown in any help commands
 */

/**
 * Class for containing and utilizing commands.
 * @class
 */
class Command {
    /**
     * @constructs
     * @param {Function} cmd The function for the command
     * @param {CommandOptions} options Options for the command
     * @param {Command} [parent={}] The Command object of the parent command
     */
    constructor(cmd, options, parent={}) {
        this.cmd = cmd;

        // Parsing the CommandOptions
        this.cid = this.determineValue(-1, parent.cid, options.cid);
        this.name = options.name;
        this.usage = options.usage;
        this.description = options.description;
        this.fullDescription = this.determineValue(this.description, undefined, options.fullDescription);
        this.category = this.determineValue(constants.permissions.public, parent.category, options.category);
        this.deleteCommand = this.determineValue(false, parent.deleteCommand, options.deleteCommand);
        this.noArgs = this.determineValue(false, parent.noArgs, options.noArgs);
        this.noClean = this.determineValue(false, parent.noClean, options.noClean);
        this.arguments = this.determineValue({}, undefined, options.arguments);
        this.help = this.determineValue(undefined, parent.help, options.help);

        // Holding a copy of the parent command if applicable
        if (options.parent) {
            this.parent = parent;
        }
    }

    /**
     * Determine what value should be assigned given default, inherited, and requested options
     * @private
     * @param {*} def The default value
     * @param {*} parent The inherited value
     * @param {*} set The request value
     */
    determineValue(def, parent, set) {
        if (typeof set !== "undefined") {
            return set;
        } else if (typeof parent !== "undefined") {
            return parent;
        } else {
            return def;
        }
    }

    /**
     * Registers a child command to this object.
     * @param {Function} cmd the function for the child to run
     * @param {CommandOptions} options options for the child command
     */
    registerChild(cmd, options) {
        // Making sure the children Object exists
        if (!this.children) {
            this.children = {};
            this.childAliases = {};
        }

        if (options.name in this.children || options.name in this.childAliases) {
            throw new Error(`Subcommand by the name ${options.name} already exists!`);
        }

        this.children[options.name] = new Command(cmd, options, this);
        if (options.aliases) {
            for (var alias of options.aliases) {
                this.childAliases[alias] = this.children[options.name];
            }
        }
    }

    /**
     * Executes the command on the given message.
     * @param {Manager} manager The Manager instance which is invoking the command
     * @param {Message} msg The Message object that triggered the command
     * @param {Array<string>} args Message content split at spaces with command name removed
     * @returns {void}
     */
    exec(manager, msg, args) {
        // Checking if any subcommands need to be called instead
        if (this.children) {
            if (args[0] in this.children) {
                // Remove this command's name from args
                let child = args.shift();

                return this.children[child].exec(manager, msg, args)
            } else if (args[0] in this.childAliases) {
                // Remove this command's name from args
                let child = args.shift();

                return this.childAliases[child].exec(manager, msg, args)
            }
        }

        // Cleaning the arguments
        if (!this.noClean) {
            for (var i = 0; i < args.length; i++) {
                if (args[i] == '') {         
                    args.splice(i, 1);
                    i--;
                }
            }
        }

        // Checking the number of arguments
        if (this.arguments) {
            if (this.arguments.none) {
                if (args.length != 0) {
                    let reason = `Command takes no arguments; ${args.length} provided.`;
                    return msg.channel.send(constants.responses.badArguments(msg.content, reason));
                }
            } else if (this.arguments.exact) {
                if (args.length != this.arguments.exact) {
                    let reason = `Command requires exactly ${this.arguments.exact} arguments; ${args.length} provided.`;
                    return msg.channel.send(constants.responses.badArguments(msg.content, reason));
                }
            } else {
                if (this.arguments.min) {
                    if (args.length < this.arguments.min) {
                        let reason = `Command requires at least ${this.arguments.min} arguments; ${args.length} provided.`;
                        return msg.channel.send(constants.responses.badArguments(msg.content, reason));
                    }
                }

                if (this.arguments.max) {
                    if (args.length > this.arguments.max) {
                        let reason = `Command requires less than ${this.arguments.max} arguments; ${args.length} provided.`;
                        return msg.channel.send(constants.responses.badArguments(msg.content, reason));
                    }
                }
            }
        }

        // No (valid) children; Process the command
        if (this.noArgs) {
            this.cmd(manager, msg);
        } else {
            this.cmd(manager, msg, args);
        }
    }
}

module.exports = Command;
