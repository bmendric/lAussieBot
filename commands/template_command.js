const constants = require.main.require("./config/constants.js");

function sample(manager, msg, args) {
    console.log("sample");
}

module.exports = {
    exec: sample,
    options: {
        cid: -1,
        name: "sample",
        aliases: ["Optional"],
        usage: "sample [...]",
        description: "Sample/template command",
        fullDescription: "Optional",
        category: constants.permissions.public,
        deleteCommand: false,
        noArgs: false,
        noClean: false,
    },
}