const constants = require.main.require("./config/constants.js");

function handleReaction(user, reaction) {
    this.edit(`${user}, you added the reaction: \n\`\`\`${reaction.emoji.toString()}\`\`\``);
}

function plaintext(manager, msg, args) {
    msg.reply(`you sent me the following:\n\`\`\`\n${args.join(" ").replace(/[<>]/g, "")}\`\`\``)
    .then(message => {
        manager.handler.track(message, [{
            default: true,
            handle: handleReaction,
        }], { deleteAfterReact: true, timeout: constants.epoch.minute * 10 });
    });
}

module.exports = {
    exec: plaintext,
    options: {
        cid: 1,
        name: "plaintext",
        usage: "plaintext [...]",
        description: "Returns the same message in plaintext",
        category: constants.permissions.admin,
        deleteCommand: true,
        arguments: { min: 1 },
    },
}