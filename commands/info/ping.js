/**
 * Informs the channel of the current latency to the bot
 * @param {Manager} manager Instance of Manager which invoked the command
 * @param {Message} msg Instance of Message which called the command
 */
function ping(manager, msg) {
    msg.reply(`latency to me is ${Date.now() - msg.createdAt}ms!`);
}

module.exports = {
    exec: ping,
    options: {
        cid: 2,
        name: "ping",
        usage: "ping",
        description: "Reply to the user author with the current network ping.",
        deleteCommand: true,
        noArgs: true,
        arguments: { none: true },
    },
};
