const Command = require("./Command.js");
const Compliment = require("./utils/Compliments.js");
const constants = require("./config/constants.js");
const Handler = require("./Handler.js");

/**
 * Class for managing all bot commands
 * @class
 */
class Manager {
    /**
     * @constructs
     * @param {Client} client The running Discord.js Client
     * @param {Object} config Configuration options for the Manager
     */
    constructor(client, config, cmdList) {
        console.log("Constructing Manager...");

        this.client = client;
        this.token = config.discord;

        // Prepping the client
        console.log("Attaching hooks to the client...");
        this.client.on("ready", () => {
            this.ready();
        });
        
        this.client.on("message", message => {
            this.onMessage(message);
        });

        this.client.on("guildMemberAdd", member => {
            this.newUser(member);
        });

        console.log("Attempting client login...");
        client.login(this.token);

        // Reading config options
        console.log("Reading config data...");
        this.status = config.status || {};
        this.ignoreBots = config.ignoreBots || false;

        this.prefix = config.prefix;
        if (typeof this.prefix === "string") {
            this.prefix = Array(this.prefix);
        }

        // Registering commands
        console.log("Registering commands...");
        this.commands = {};
        this.aliases = {};
        for (let cmd of cmdList.commands) {
            let file = require(`./commands/${cmd}`);
            this.register(file.exec, file.options);
        }

        // Register children
        console.log("Registering subcommands...");
        for (let child of cmdList.subcommands) {
            let file = require(`./commands/${child}`);

            if (!file.options.parent in this.commands) {
                console.log(`Subcommand ${file.options.name} does not have a valid parent!`);
                continue;
            }
            this.commands[file.options.parent].registerChild(file.exec, file.options);
        }

        // Starting the Compliment generator
        this.compliment = new Compliment();

        // Starting the reactions handler
        this.handler = new Handler(this.client);

        console.log("Manager construction complete!");
    }

    /**
     * Function called when the client sends "ready" signal
     */
    ready() {
        console.log("Client is now logged in...");
    }

    /**
     * Registers a command with the Manager
     * @param {Function} cmd The function to be run for the command
     * @param {Object} options Non-default options for the command
     */
    register(cmd, options) {
        if (!options.cid) {
            throw new Error(`A cid is not given for ${options.name}!`);
        }

        if (options.name in this.commands || options.name in this.aliases) {
            throw new Error(`Command by the name ${options.name} already exists!`);
        }

        this.commands[options.name] = new Command(cmd, options);
        if (options.aliases) {
            for (var alias of options.aliases) {
                this.aliases[alias] = this.commands[options.name];
            }
        }
    }

    /**
     * Checks if the content is prefixed by a valid prefix
     * @private
     * @param {string} content The content of the current Message
     * @returns {?string} The prefix that was matched; undefined if none found
     */
    checkPrefix(content) {
        for (var x of this.prefix) {
            if (content.substr(0, x.length) === x) {
                return x;
            }
        }
        return undefined;
    }

    /**
     * Checks if the message should be ignored
     * @private
     * @param {Message} msg Message to be checked
     * @returns {bool} True if the Message should be processed; false otherwise
     */
    checkIgnore(msg) {
        if (this.ignoreBots && msg.author.bot) {
            return false;
        }
        return true;
    }

    /**
     * Command for processing incoming messages
     * @param {Message} msg The message being processed
     * @returns {void}
     */
    onMessage(msg) {
        // Checking if the message came from a text channel
        if (msg.channel.type != "text") {
            return;
        }

        var current_prefix = this.checkPrefix(msg.content);
        // Checking if we care about the message
        if (!current_prefix || !this.checkIgnore(msg)) {
            return;
        }

        // Creating args list
        var args = msg.content.split(" ");

        // Figuring out which command the message is invoking
        var command = args.shift();
        command = command.replace(new RegExp(`^${current_prefix}`), "");
        command = command.toLowerCase();

        // Finding the Command object for the command
        var cmdObj;
        if (command in this.commands) {
            cmdObj = this.commands[command];
        } else if (command in this.aliases) {
            cmdObj = this.aliases[command];
        }

        // Running the command if one was found
        if (cmdObj) {
            cmdObj.exec(this, msg, args);

            // Checking if the original message should be deleted
            if (cmdObj.deleteCommand) {
                msg.delete();
            }
        }
    }

    /**
     * Performs initial communication with a new user
     * @param {GuildMember} user The user who became reachable to the bot
     */
    newUser(guildMember) {
        var this_ = this;

        // Adding the user to the database
        this.db.run(`INSERT OR IGNORE INTO PLAYERS(ID) VALUES(${guildMember.id})`);

        // Sending the user introductory info
        guildMember.createDM()
        .then(dmChannel => {
            return dmChannel.send(constants.responses.welcome(guildMember.user));
        })
        .then(undefined, err => {
            this_.getDefaultChannel(guildMember.guild)
            .then(guildChannel => {
                guildChannel.send(constants.responses.welcomeBackup(guildMember.user));
            });
        });
    }

    /**
     * Determines a makeshift "default" text channel for a given guild
     * https://github.com/AnIdiotsGuide/discordjs-bot-guide/blob/master/frequently-asked-questions.md
     * @param {Guild} guild A Guild structure
     * @returns {Promise<TextChannel>}
     */
    getDefaultChannel(guild) {
        return new Promise((resolve, reject) => {
            // get "original" default channel
            if (guild.channels.has(guild.id)) {
                resolve(guild.channels.get(guild.id));
            }

            // Check for a "general" channel, which is often default chat
            if (guild.channels.exists("name", "general")) {
                resolve(guild.channels.find("name", "general"));
            }
            
            // Now we get into the heavy stuff: first channel in order where the bot can speak
            resolve(guild.channels
                .filter(c => c.type === "text" &&
                    c.permissionsFor(guild.client.user).has("SEND_MESSAGES"))
                .sort((a, b) => {
                    a.position - b.position || Long.fromString(a.id).sub(Long.fromString(b.id)).toNumber()
                })
                .first()
            );
        });
    }
}

module.exports = Manager;
